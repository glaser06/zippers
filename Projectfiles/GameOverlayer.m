//
//  GameOverlayer.m
//  ZippersZippers
//
//  Created by Zaizen Kaegyoshi on 6/21/13.
//
//

#import "GameOverlayer.h"
#import "GameLayer.h"
#import "Zipper.h"
#import "StartLayer.h"
@implementation GameOverlayer
-(id)init
{
    if((self = [super init]))
    {
        glClearColor(.5f, 0, 0, .7f);
        zipperButtons = [NSMutableArray array];
        int score = [[[NSUserDefaults standardUserDefaults]objectForKey:@"highscore1"]intValue];
        
        CCLabelTTF *welcomeSign = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i",score] fontName:@"Georgia" fontSize:50];
        [self addChild:welcomeSign];
        welcomeSign.position = ccp(100,480);
        CCLabelTTF *welcomeSign2 = [CCLabelTTF labelWithString:@"Statistics" fontName:@"Georgia" fontSize:25];
        welcomeSign2.position = ccp(260,550);
        [self addChild:welcomeSign2];
        
        float timeForGame = [[[NSUserDefaults standardUserDefaults]objectForKey:@"timePerGame"]floatValue]/60;
        CCLabelTTF *timeStatistics = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%.1f zippers/s",(score/timeForGame)] fontName:@"Georgia" fontSize:15];
        timeStatistics.position=ccp(250,520);
        [self addChild:timeStatistics];
        
        double totalZipperLength = [[[NSUserDefaults standardUserDefaults]objectForKey:@"totalZipperLength"]doubleValue];
        double inches = totalZipperLength/14.17;
        CCLabelTTF *cmOfZippers = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%.2f cm of zippers\n unzipped",inches] fontName:@"Georgia" fontSize:15];
        cmOfZippers.position = ccp(250,490);
        [self addChild:cmOfZippers];
        
        
        
        //Zipper buttons
        playButton = [Zipper zipper:ccp(80,180) end:ccp(240,181) precision:20];
        [self addChild:playButton z:1 tag:10];
        [zipperButtons addObject:playButton];
        CCLabelTTF *replayTxt = [CCLabelTTF labelWithString:@"Play Again" fontName:@"Georgia" fontSize:25];
        replayTxt.position=ccp(abs((playButton.end.x+playButton.start.x)/2),180);
        [self addChild:replayTxt];
        
        menuButton = [Zipper zipper:ccp(80,100) end:ccp(240,101) precision:20];
        [self addChild:menuButton z:1 tag: 11];
        [zipperButtons addObject:menuButton];
        CCLabelTTF *menuTxt = [CCLabelTTF labelWithString:@"Main Menu" fontName:@"Georgia" fontSize:25];
        menuTxt.position=ccp(abs((menuButton.end.x+menuButton.start.x)/2),100);
        [self addChild:menuTxt];
        
        
//        CCLabelTTF *playText = [CCLabelTTF labelWithString:@"Play" fontName:@"Georgia" fontSize:30];
//        playText.position = ccp(abs((playButton.end.x+playButton.start.x)/2),300);
//        [self addChild:playText];
        
        [self scheduleUpdate];
        
        
    }
    return self;
}

-(void)update:(ccTime)delta
{
    KKInput *input = [KKInput sharedInput];
    for(int i=0;i<(int)[zipperButtons count];i++)
    {
        Zipper *temp = [zipperButtons objectAtIndex:i];
        
        if(temp.unzipped == true)
        {
            if(CGPointEqualToPoint(temp.end, playButton.end))
            {
                [self gameLayerChange];
            }
            if(CGPointEqualToPoint(temp.end, menuButton.end))
            {
                [self menuLayerChange];
            }
            [self removeChild:temp];
            [zipperButtons removeObject:temp];
            
            
            //[unzipped addObject:temp];
            
            
        }
        
    }
    if([input anyTouchBeganThisFrame])
    {
        touch = input.anyTouchLocation;
        for(int i=0;i<(int)[zipperButtons count];i++)
        {
            Zipper *temp = [zipperButtons objectAtIndex:i];
            if([self checkTouch:temp precision:50 precision:50])
            {
                zip = temp;
                //[self addChild:zip];
            }
        }
    }
    else if([input touchesAvailable])
    {
        touch = input.anyTouchLocation;
        if([self checkTouch:zip precision:150 precision:150])
        {
            [zip unzip:touch];
        }
    }
    
    
}
-(void)gameLayerChange
{
    [self pauseSchedulerAndActions];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLayer scene]]];
}
-(void)menuLayerChange
{
    [self pauseSchedulerAndActions];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[StartLayer scene]]];
}

-(void)draw
{
    
    for(int i=0;i<(int)[zipperButtons count];i++)
    {
        Zipper *temp = [zipperButtons objectAtIndex:i];
        if(temp.unzipped == false)
        {
            [temp drawLine:self];
        }
        
        
    }
    
    
    
    
    //
    //    if(!CGPointEqualToPoint(touch, CGPointZero))
    //        acorn.position = touch;
}


-(bool)checkTouch:(Zipper*)zipper precision:(int)width precision:(int)length
{
    CGPoint cur = zipper.position;
    if(touch.x<=cur.x+width&&touch.x>=cur.x-width)
    {
        if(touch.y<=cur.y+length&&touch.y>=cur.y-length)
        {
            return true;
        }
    }
    
    return false;
}

+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameOverlayer *layer = [GameOverlayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

@end
