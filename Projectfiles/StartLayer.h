//
//  StartLayer.h
//  ZippersZippers
//
//  Created by Zaizen Kaegyoshi on 6/21/13.
//
//

#import "CCLayer.h"
#import "Zipper.h"
@interface StartLayer : CCLayer
{
    CGPoint touch;
    NSMutableArray *zipperButtons;
    NSMutableArray *zipperButtonImgs;
    Zipper *zip;
    Zipper *playButton;
}
+(id) scene;

@end
