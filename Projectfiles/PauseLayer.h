//
//  PauseLayer.h
//  ZippersZippers
//
//  Created by Zaizen Kaegyoshi on 6/22/13.
//
//

#import "CCLayer.h"
#import "StartLayer.h"
#import "GameLayer.h"
#import "Zipper.h"
@interface PauseLayer : CCLayer
{
    CGPoint touch;
    NSMutableArray *zipperButtons;
    NSMutableArray *zipperButtonImgs;
    Zipper *zip;
    Zipper *resumeButton;
    Zipper *restartButton;
    Zipper *menuButton;
    
}
+(id) scene;
@end
