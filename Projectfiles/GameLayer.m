/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim.
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "GameLayer.h"
#import "Zipper.h"
#import "GameOverlayer.h"
#import "PauseLayer.h"
@implementation GameLayer

-(id) init
{
	if ((self = [super init]))
	{
        zip=NULL;
        levelSolved = true;
        finalLevel=false;
        tutorial = true;
        time = 659;
        level =0;
        score = 0;
        realTime=0;
        difficulty = 1;
		glClearColor(0.2f, 0.1f, 0.3f, 1.0f);
        zippers = [NSMutableArray array];
        unzipped = [NSMutableArray array];
        points  = [NSMutableArray array];
        nextLevel = [NSMutableArray array];
        zipperLines = [NSMutableArray array];
        unzippedLines = [NSMutableArray array];
        totalZipperLength=0;
        countdown=239;
        
//        for(int i=0;i<320;i++)
//        {
//            for(int j=0;j<480;j++)
//            {
//                [points addObject:[NSNumber valueWithCGPoint:ccp(i,j)]];
//            }
//        }
        CCLabelTTF *timeLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i",(int)time/60] fontName:@"Georgia" fontSize:20];
        timeLabel.position = ccp(300,530);
        [self addChild:timeLabel z:1 tag:1];
        CCLabelTTF *levelLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i",level] fontName:@"Georgia" fontSize:20];
        levelLabel.position = ccp(300,550);
        [self addChild:levelLabel z:1 tag:2];
        CCLabelTTF *scoreLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i",score] fontName:@"Georgia" fontSize:20];
        [self addChild:scoreLabel z:1 tag:3];
        scoreLabel.position=ccp([[CCDirector sharedDirector]screenCenter].x,530);
        CCLabelTTF *pause = [CCLabelTTF labelWithString:@"pause" fontName:@"Arial" fontSize:20];
        CCLabelTTF *resume = [CCLabelTTF labelWithString:@"pause" fontName:@"Arial" fontSize:20];
        CCMenuItemLabel *pauseItem = [CCMenuItemLabel itemWithLabel:pause target:self selector:@selector(pauseLayerChange)];
        pauseItem.position= ccp(50,550);
        CCLabelTTF *countdownLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i",countdown] fontName:@"Georgia" fontSize:40];
        countdownLabel.position = [[CCDirector sharedDirector]screenCenter];
        [self addChild:countdownLabel z:2 tag:9001];
        CCMenuItemLabel *resumeItem = [CCMenuItemLabel itemWithLabel:resume target:self selector:@selector(pause)];
        resumeItem.position = ccp(50,530);
        CCMenu *menu = [CCMenu menuWithItems:pauseItem,resumeItem, nil];
        menu.position = ccp(0,0);
        [self addChild:menu];
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithDouble:totalZipperLength] forKey:@"totalZipperLength"];
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:0] forKey:@"highscore1"];
        [self scheduleUpdate];
        
        
	}
    
	return self;
}

-(void)update:(ccTime)delta
{
    if(countdown>0)
    {
        CCLabelTTF *countdownLabel = (CCLabelTTF*)[self getChildByTag:9001];
        [countdownLabel setString:[NSString stringWithFormat:@"%i",countdown/60]];
        countdown--;
        
    }
    
    else
    {
        if(countdown==0)
        {
            [self removeChildByTag:9001];
            countdown--;
        }
        //CGPoint x = [points objectAtIndex:0];
        CCLabelTTF *temp = (CCLabelTTF*)[self getChildByTag:1];
        
        [temp setString:[NSString stringWithFormat:@"%i",(int)time/60]];
        
        
        
        CCLabelTTF *tempLevel = (CCLabelTTF*) [self getChildByTag:2];
        if(!finalLevel)
            [tempLevel setString:[NSString stringWithFormat:@"%i",level]];
        if(finalLevel)
            [tempLevel setString:@"UNZIP!!!!!!!"];
        input = [KKInput sharedInput];
        //unzipping
        [self unzipping];
        
        if([input anyTouchBeganThisFrame])
        {
            touch = input.anyTouchLocation;
            for(int i=0;i<(int)[zippers count];i++)
            {
                Zipper *temp = [zippers objectAtIndex:i];
                int precisionx = 50;
                int precisiony = 39;
                if(finalLevel)
                {
                    precisionx = 65;
                    precisiony = 65;
                }
                if([self checkTouch:temp precision:precisionx precision:precisiony])
                {
                    zip = temp;
                    //[self addChild:zip];
                }
            }
        }
        else if([input touchesAvailable])
        {
            touch = input.anyTouchLocation;
            if([self checkTouch:zip precision:150 precision:150])
            {
                //CGPoint curzipPos = zip.position;
                [zip unzip:touch];
                //            if(![self checkZipperMove])
                //            {
                //                zip.position=curzipPos;
                //            }
            }
        }
        
        {
            [self tutorialLevel];
        }
        //int random = arc4random()%20+30;
        
        //next level generator
        for(int i=0;i<2;i++)
        {
            [self nextLevelGenerator];
            [self nextLevelGenerator];
        }
        
        if(finalLevel)
        {
            if([zippers count]==0)
                [self gameOverChange];
            [self finalLevelGenerator];
        }
        //game over check
        [self gameOverCheck];
        time--;
        realTime++;
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:realTime ] forKey:@"timePerGame"];
        
        
    }
    
}

-(void)tutorialLevel
{
    if(tutorial)
    {
        CCLabelTTF *instruction1 = [CCLabelTTF labelWithString:@"Drag the Zipper from \n beginning to end" fontName:@"Georgia" fontSize:20];
        instruction1.position = ccp(150,50);
        [self addChild:instruction1 z:10 tag:1001];
        tutorial = false;
        
    }
    else
    {
        if(level>1)
        {
            [self removeChildByTag:1001];
            
        }
        
    }
    
    

}

//-(bool)checkZipperMove
//{
//    for(int i=0;i<(int)[zipperLines count];i++)
//    {
//        CCSprite *zipLine = [zipperLines objectAtIndex:i];
//        CGRect zipLineRect = [[zip zipperLine]boundingBox];
//        CGRect tempRect = [zipLine boundingBox];
//        CGRect zipRect = [zip boundingBox];
//        
//        ccDrawColor4B(255, 199, 199, 255);
//        ccDrawCircle(zipRect.origin, 20, 0, 50, NO);
//        ccDrawCircle(tempRect.origin, 20, 0, 50, NO);
//        ccDrawCircle(zipLineRect.origin, 20, 0, 50, NO);
//        if(!CGRectEqualToRect(zipLineRect, tempRect))
//        {
//            if(CGRectIntersectsRect(tempRect, zipRect))
//            {
//                return false;
//            }
//        }
//    }
//    
//    return true;
//
//}



-(void)nextLevelGenerator
{
    if(!finalLevel)
    {
        int random = arc4random()%30+20;
        Zipper *nextZipper = [Zipper zipper];
        if([self generateZipper:nextZipper]&&(int)[nextLevel count]<(difficulty+1)&&(int)[nextLevel count]<random)
        {
            [nextLevel addObject:nextZipper];
        }
        
        if(levelSolved==true)
        {
            [self levelGenerator];
            float rand = arc4random()%10;
            float rand1 = arc4random()%10;
            float rand2 = arc4random()%10;
            float rand3 = arc4random()%10;
            glClearColor(rand/10, rand1/10, rand2/10, rand3/10);
        }
        if([zippers count]==0)
        {
            levelSolved=true;
            time+=[nextLevel count]*(30-[nextLevel count]);
        }
    }

}
-(bool)generateZipper:(Zipper*)zipper
{
    for(int i=0;i<(int)[nextLevel count];i++)
    {
        Zipper *tempZip = [nextLevel objectAtIndex:i];
        CGRect zipRect = [zipper boundingBox];
        CGRect tempRect = [tempZip boundingBox];
//        CGRect zipLine = [[zipper zipperLine]boundingBox];
//        CGRect tempZipLine = [[tempZip zipperLine]boundingBox];
        if(CGRectIntersectsRect(tempRect, zipRect))
        {
            return false;
        }
        if([self checkIntersection:zipper zipper2:tempZip])
        {
            if([zipper slope]*[tempZip slope]!=-1)
                return false;
        }
//        if(CGRectContainsPoint(tempZipLine, zipper.position))
//        {
//            return false;
//        }
    }
    return true;
    
}

-(bool)checkIntersection:(Zipper*)zipper1 zipper2:(Zipper*)zipper2
{
    
    int x1 = zipper1.start.x;
    int y1 = zipper1.start.y;
    int x2 = zipper1.end.x;
    int y2 = zipper1.end.y;
    int a1 = zipper2.start.x;
    int b1 = zipper2.start.y;
    int a2 = zipper2.end.x;
    int b2 = zipper2.end.y;
//    if(x1<x2)
//    {
//        x1-=40;
//    }
//    else{x1+=40;}
//    if(y1<y2)
//    {
//        y1-=60;
//    }
//    else {y1+=40;}
//    if(a1<a2)
//    {
//        a1-=40;
//    }
//    else{a1+=40;}
//    if(b1<b2)
//    {
//        b1-=60;
//    }
//    else {b1+=40;}
    int cross1 = (x1-x2)*(b1-y1)-(y1-y2)*(a1-x1);
    int cross2 = (x1-x2)*(b2-y1)-(y1-y2)*(a2-x1);
    if((cross1<0&&cross2<0)||(cross1>0&&cross2>0))
    {
        return false;
    }
    return true;
}


-(void)unzipping
{
    CCLabelTTF *scoreLabel = (CCLabelTTF*)[self getChildByTag:3];
    //[scoreLabel setString:[NSString stringWithFormat:@"%i",score]];
    for(int i=0;i<(int)[zippers count];i++)
    {
        Zipper *temp = [zippers objectAtIndex:i];
        CCSprite *tempLine = [zipperLines objectAtIndex:i];
        if(temp.unzipped == true)
        {
            if([temp extendedTime])
            {
                time+=60;
            }
            [self removeChild:temp];
            
            [self removeChild:tempLine];
            
            [zippers removeObject:temp];
            [zipperLines removeObject:tempLine];
            totalZipperLength+=[temp length];
            if(!finalLevel)
            {
                [unzipped addObject:temp];
                [unzippedLines addObject:tempLine];
            }
            score++;
            [scoreLabel setString:[NSString stringWithFormat:@"%i",score]];
            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInt:score] forKey:@"highscore1"];
            [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithDouble:totalZipperLength] forKey:@"totalZipperLength"];
            //[points removeObjectsInArray:[temp line]];
            //[self removeChildByTag:10];
            
        }
        
    }
    
}
-(void)gameOverCheck
{
    if(time<=0&&!finalLevel)
    {
        finalLevel=true;
        [self removeZippers];
        [self finalLevelGenerator];
        time+=600;
        //        nextLevel = unzipped;
        //        [self levelGenerator];
        //        time = 600;
        //        [unzipped removeAllObjects];
        
    }
    else if(time<=0&&finalLevel)
    {
        
        
        [self gameOverChange];
    }

}
-(void)levelGenerator
{
//    Zipper *newZip = [Zipper zipper:ccp(100,100) end:ccp(200,200)];
//    Zipper *newZip2 = [Zipper zipper:ccp(100,200) end:ccp(200,100)];
//    [zippers addObject:newZip];
//    [zippers addObject:newZip2];
//    [self addChild:newZip z:1];
//    [self addChild:newZip2 z:2];
    for(int i=0;i<(int)[nextLevel count];i++)
    {
        Zipper *newZip = [nextLevel objectAtIndex:i];
        [zippers addObject:newZip];
        CCSprite *zipLine = [newZip drawZipperLine];
        [self addChild:zipLine z:[newZip layer]];
        [zipperLines addObject:zipLine];
        [self addChild:newZip z:[newZip layer]];
        newZip.unzipped = false;
        
    }
    [nextLevel removeAllObjects];
    
    levelSolved = false;
    level++;
    difficulty+=2;
    
}
-(void)finalLevelGenerator
{
    
    for(int i=0;i<(int)[unzipped count]&&[zippers count]<2;i++)
    {
        Zipper *newZip = [Zipper zipper:ccp(160,160) end:ccp(161,376) precision:80];
        CCSprite *newZipLine = [newZip drawZipperLine];
        [zippers addObject:newZip];
        [zipperLines addObject:newZipLine];
        [self addChild:newZipLine];
        [self addChild:newZip];
        [unzipped removeObjectAtIndex:i];
        
    }
    
    
//    for(int i=0;(i<(int)[unzipped count]&&[zippers count]<25);i++)
//    {
//        Zipper *newZip = [unzipped objectAtIndex:i];
//        CCSprite *newZipLine = [unzippedLines objectAtIndex:i];
//        [zippers addObject:newZip];
//        [zipperLines addObject:newZipLine];
//        [self addChild:newZipLine];
//        [self addChild:newZip z:[newZip layer]];
//        newZip.unzipped=false;
//        newZip.position=[newZip start];
//        [unzipped removeObject:newZip];
//        [unzippedLines removeObject:newZipLine];
//    }
    
    finalLevel = true;
}
-(void)removeZippers
{
    for(int i=0;i<(int)[zippers count];i++)
    {
        Zipper *temp = [zippers objectAtIndex:i];
        CCSprite *tempLine = [zipperLines objectAtIndex:i];
        {
            [self removeChild:temp];
            [self removeChild:tempLine];
            
            
        }
    }
    [zippers removeAllObjects];
    [zipperLines removeAllObjects];
}
-(void)draw
{
    
    for(int i=0;i<(int)[zippers count];i++)
    {
        Zipper *temp = [zippers objectAtIndex:i];
        if(temp.unzipped == false)
        {
            [temp drawLine:self];
        }
        
        
    }

}

-(bool)checkTouch:(Zipper*)zipper precision:(int)width precision:(int)length
{
    CGPoint cur = zipper.position;
    if(touch.x<=cur.x+width&&touch.x>=cur.x-width)
    {
        if(touch.y<=cur.y+length&&touch.y>=cur.y-length)
        {
            return true;
        }
    }
    
    return false;
}


//game layer functions
-(void)pause
{
    [self pauseSchedulerAndActions];
}
-(void)resume
{
    [self resumeSchedulerAndActions];
}
-(void)pauseLayerChange
{
    
    [[CCDirector sharedDirector]pushScene:[CCTransitionFade transitionWithDuration:.6f scene:[PauseLayer scene]]];
}
-(void)restart
{
    [self pauseSchedulerAndActions];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLayer scene]]];
}
-(void)gameOverChange
{
    [self pauseSchedulerAndActions];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameOverlayer scene]]];
}
+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameLayer *layer = [GameLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}


@end
