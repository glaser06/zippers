//
//  Zipper.h
//  ZippersZippers
//
//  Created by Zaizen Kaegyoshi on 6/7/13.
//
//

#import <Foundation/Foundation.h>
//#import "ZipperLine.h"
@interface Zipper : CCSprite
{
    
    
   
    
    //ZipperLine *zipLine;
}
@property bool unzipped;
@property bool zip;
@property NSMutableArray *line;
@property CGPoint start;
@property CGPoint end;
@property bool extendedTime;
@property int layer;
@property CCSprite *zipperLine;
@property double slope;
@property int precision;
@property double length;
@property CCSpriteBatchNode *teethBatch;

+(id)zipper;
+(id)zipper:(CGPoint)start end:(CGPoint) end precision:(int)prec;
-(CGPoint)generatePoint;
-(id)initWithZipperImage;
-(id) initWithZipperImage:(CGPoint)s end:(CGPoint)e precision:(int)prec;
-(void)drawLine:(CCLayer*)gameLayer;
-(CGPoint)unzip:(CGPoint) touch;
//-(bool)checkTouch:(CGPoint)touch;
-(CCSprite*)drawZipperLine;

@end
