/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim. 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

#import "kobold2d.h"
#import <Foundation/Foundation.h>
#import "Zipper.h"
@interface GameLayer : CCLayer
{
    Zipper *acorn;
    CGPoint touch;
    KKInput *input;
    float time;
    Zipper *zip;
    NSMutableArray *zippers;
    NSMutableArray *unzipped;
    NSMutableArray *points;
    NSMutableArray *nextLevel;
    NSMutableArray *zipperLines;
    NSMutableArray *unzippedLines;
    bool levelSolved;
    int level;
    int difficulty;
    bool finalLevel;
    int score;
    int realTime;
    double totalZipperLength;
    int countdown;
    bool tutorial;
    //CCLabelTTF *timeLabel;
}
-(bool)checkTouch:(Zipper*)zipper precision:(int) width precision:(int)length;
-(void)restart;
+(id)scene;

@end
