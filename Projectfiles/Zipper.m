//
//  Zipper.m
//  ZippersZippers
//
//  Created by Zaizen Kaegyoshi on 6/7/13.
//
//

#import "Zipper.h"
#import "GameLayer.h"
@implementation Zipper
#define slope(x,y,x1,y1) (y1-y)/(x1-x)
#define d(p1x,p1y,p2x,p2y) sqrt((p1x - p2x) * (p1x - p2x) + (p1y - p2y) * (p1y - p2y))
@synthesize unzipped;
@synthesize zip;
@synthesize line;
@synthesize start;
@synthesize end;
@synthesize extendedTime;
@synthesize layer;
@synthesize zipperLine;
@synthesize slope;
@synthesize precision;
@synthesize length;
@synthesize teethBatch;
+(id) zipper
{
    id zipper = [[self alloc]initWithZipperImage];
    return zipper;
}
+(id)zipper:(CGPoint)start end:(CGPoint)end precision:(int) prec
{
    id zipper = [[self alloc]initWithZipperImage:start end:end precision:prec];
    return zipper;
}
-(id) initWithZipperImage
{
    if((self = [super initWithFile:@"zipper.png"]))
    {
        line = [NSMutableArray array];
        int rand = arc4random()%15;
        //        if(rand==1) zip = true;
        //        if(rand==2) zip=false;
        //        if(zip)
        //        {
        //            self.flipX=TRUE;
        //        }
        precision =50;
        layer = rand;
        unzipped =false;
        
        if(rand<1)
        {
            extendedTime = true;
        }
        [self initLine];
        //[self addPoints:points];
        //[self erasePoints:points];
        //[self generateLine:points];
        //start = [[line objectAtIndex:0]CGPointValue];
        self.position=start;
        //self drawLine];
        [self drawZipperLine];
        slope = (start.y-end.y)/(start.x-end.x);
        float angle = CC_RADIANS_TO_DEGREES(atan((start.y-end.y)/(start.x-end.x)));
        self.rotation =180-(angle);
        if(start.x>end.x)
        {
            self.rotation+=180;
        }
        
        length = d(start.x,start.y,end.x,end.y);
        
        teethBatch = [CCSpriteBatchNode batchNodeWithFile:@"zipperteeth.png"];
        
        
    }
    return self;
}
-(id) initWithZipperImage:(CGPoint)s end:(CGPoint)e precision:(int)prec
{
    
    if((self = [super initWithFile:@"zipper.png"]))
    {
        line = [NSMutableArray array];
        int rand = arc4random()%12;
        //        if(rand==1) zip = true;
        //        if(rand==2) zip=false;
        //        if(zip)
        //        {
        //            self.flipX=TRUE;
        //        }
        precision = prec;
        unzipped =false;
        if(rand<1)
        {
            //extendedTime = true;
        }
        self.start = s;
        self.end = e;
        //[self addPoints:points];
        //[self erasePoints:points];
        //[self generateLine:points];
        //start = [[line objectAtIndex:0]CGPointValue];
        self.position=start;
        //self drawLine];
        slope = (start.y-end.y)/(start.x-end.x);
        [self drawZipperLine];
        
        
        float angle = CC_RADIANS_TO_DEGREES(atan((start.y-end.y)/(start.x-end.x)));
        self.rotation =180-(angle);
        if(start.x>end.x)
        {
            self.rotation+=180;
        }
        length = d(start.x,start.y,end.x,end.y);
        
    }
    return self;
}

-(void)initLine
{
    start = [self generatePoint];
    end = [self generatePoint];
    //[self generateLine];
    int dist = d(start.x,start.y,end.x,end.y);
    //double slope = (start.y-end.y)/(start.x-end.x);
    
    if(dist<100||start.x==end.x||dist>250)
    {
        [self initLine];
    }
    
        
    
    
    
}
-(bool)checkStartPoint:(NSMutableArray*)points
{
    if([points containsObject:[NSNumber valueWithCGPoint:start]])
    {
        return false;
    }
    
        
    return true;
}
-(void)addPoints:(NSMutableArray*)points
{
    
    
    CGPoint temp = start;
    for(int x = temp.x-30;x<temp.x+30;x++)
    {
        for(int y = temp.y-30;y<temp.y+30;y++)
        {
            [points addObject:[NSNumber valueWithCGPoint:ccp(x,y)]];
            [line addObject:[NSNumber valueWithCGPoint:ccp(x,y)]];
        }
    }
    

}
-(void)drawLine:(CCLayer *) gameLayer
{
    
    
    ccDrawColor4F(.9f, .9f, .9f, .9f);
    //ccDrawLine(start, end);
    if( extendedTime)
    {
        //ccDrawColor4B(255, 211, 100, 200);
        ccDrawCircle(self.position, 30, 0, 50, NO);
    }
    
    
}
-(CCSprite*)drawZipperLine
{
    
    CGPoint p0 = self.position;
    CGPoint p1 = end;
    
    //CCSpriteBatchNode *batch = [CCSpriteBatchNode batchNodeWithFile:@"zipperteeth.png"];
    
    zipperLine = [CCSprite spriteWithFile:@"zipperLine.png" rect:CGRectMake(0, 0, d(p0.x, p0.y, p1.x, p1.y) , 27)];
    //CCSprite *sprite = [CCSprite spriteWithBatchNode:batch rect:CGRectMake(0, 0, d(p0.x, p0.y, p1.x, p1.y) , 30)];
    zipperLine.anchorPoint = ccp(0,.5);
    zipperLine.position = p0;
    zipperLine.flipX=true;
    zipperLine.rotation =180+self.rotation;
    //[batch addChild:sprites];
    //[self addChild:sprites ];
    //NSLog([NSString stringWithFormat:@"%fx %fy",zipperLine.anchorPoint.x,zipperLine.anchorPoint.y]);
    return zipperLine;
}
-(void)drawZipperTeeth
{
    // CCSprite *teeth = [CCSprite spriteWithBatchNode:teethBatch rect:nil];
    
}
-(CGPoint)generatePoint
{
    int x;
    int y;
    x = arc4random()%240 + 30;
    y = arc4random()%440 +30;
    return ccp(x,y);
    
    //    int rand = arc4random()%[points count];
    //    return [[points objectAtIndex:rand]CGPointValue];
    
    
}

-(CGPoint)unzip:(CGPoint) touch
{
    CGPoint cur = self.position;
    if(end.x<=cur.x+precision&&end.x>=cur.x-precision)
    {
        if(end.y<=cur.y+precision&&end.y>=cur.y-precision)
        {
            unzipped = true;
            [zipperLine removeFromParent];
        }
    }
    
    //    if(((touch.x<=start.x&&touch.x>end.x)||(touch.x>=start.x&&touch.x<=end.x))&&((touch.y<=start.y&&touch.y>=end.y)||(touch.y>=start.y&&touch.y<=end.y)))
    //zipperLine.rotation+=1;
    double slope1 = (start.y-end.y)/(start.x-end.x);
    double y,x;
    
    
    if(abs(slope1)>=1)
    {
        x = (touch.y-start.y)/slope1+start.x;
        if((x<=start.x&&x>=end.x)||(x<=end.x&&x>=start.x))
            self.position = ccp(x,touch.y);
    }
    else if(abs(slope1)<1)
    {
        y = slope1*(touch.x-start.x)+start.y;
        if((y<=start.y&&y>=end.y)||(y<=end.y&&y>=start.y))
            self.position = ccp(touch.x,y);
        
    }
//    CGPoint p0 = self.position;
//    CGPoint p1 = end;
//    int distance = d(p0.x, p0.y, p1.x, p1.y);
    //[zipperLine ];
//    float percent = distance/length;
    //CCSpriteBatchNode *batch = [CCSpriteBatchNode batchNodeWithFile:@"zipperteeth.png"];
    //zipperLine.anchorPoint=ccp(50,0+zipperLine.size.height/2);
    GameLayer *gamelayer= (GameLayer*)[zipperLine parent];
    [zipperLine removeFromParent];
    zipperLine = [self drawZipperLine];
    [gamelayer addChild:zipperLine z:zipperLine.zOrder+12];
    self.zOrder = zipperLine.zOrder+13;
    //[layer addChild:zipperLine];
    //zipperLine.scaleX=percent;
    //CCSprite *sprite = [CCSprite spriteWithBatchNode:batch rect:CGRectMake(0, 0, d(p0.x, p0.y, p1.x, p1.y) , 30)];
    //zipperLine.position = p0;
    return self.position;
    
    
    
       
    
}


@end
