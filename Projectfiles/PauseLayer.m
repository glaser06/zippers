//
//  PauseLayer.m
//  ZippersZippers
//
//  Created by Zaizen Kaegyoshi on 6/22/13.
//
//

#import "PauseLayer.h"

@implementation PauseLayer

-(id)init
{
    if((self = [super init]))
    {
        glClearColor(.5f, 0, 0, .7f);
        
        zipperButtons = [NSMutableArray array];
        zipperButtonImgs = [NSMutableArray array];
        
        CCLabelTTF *welcomeSign = [CCLabelTTF labelWithString:@"Paused" fontName:@"Georgia" fontSize:40];
        [self addChild:welcomeSign];
        welcomeSign.position = ccp(160,520);
//        CCLabelTTF *welcomeSign2 = [CCLabelTTF labelWithString:@"Zippers!!!!" fontName:@"Georgia" fontSize:45];
//        welcomeSign2.position = ccp(160,480);
//        [self addChild:welcomeSign2];
        
        
        
        //Zipper buttons
        resumeButton = [Zipper zipper:ccp(80,300) end:ccp(222,301)precision:20];
        CCSprite *resumeButtonLine = [resumeButton drawZipperLine];
        resumeButtonLine.zOrder = 1;
        [zipperButtonImgs addObject:resumeButtonLine];
        [self addChild:resumeButtonLine];
        [self addChild:resumeButton z:1 tag:10];
        [zipperButtons addObject:resumeButton];
        
        CCLabelTTF *playText = [CCLabelTTF labelWithString:@"Resume" fontName:@"Georgia" fontSize:30];
        playText.position = ccp(160,300);
        [self addChild:playText z:10];
        
        restartButton = [Zipper zipper:ccp(80,200) end:ccp(222,201)precision:20];
        CCSprite *restartButtonLine = [restartButton drawZipperLine];
        restartButtonLine.zOrder = 1;
        [zipperButtonImgs addObject:restartButtonLine];
        [self addChild:restartButtonLine];
        [self addChild:restartButton z:1 tag:10];
        [zipperButtons addObject:restartButton];
        
        CCLabelTTF *restartText = [CCLabelTTF labelWithString:@"Restart" fontName:@"Georgia" fontSize:30];
        restartText.position = ccp(160,200);
        [self addChild:restartText z:10];
        
        menuButton = [Zipper zipper:ccp(80,100) end:ccp(222,101) precision:20];
        CCSprite *menuButtonLine = [menuButton drawZipperLine];
        menuButtonLine.zOrder = 1;
        [zipperButtonImgs addObject:menuButtonLine];
        [self addChild:menuButtonLine];
        [self addChild:menuButton z:1 tag:10];
        [zipperButtons addObject:menuButton];
        
        CCLabelTTF *menuText = [CCLabelTTF labelWithString:@"Menu" fontName:@"Georgia" fontSize:30];
        menuText.position = ccp(160,100);
        [self addChild:menuText z:10];

        
        
        
        [self scheduleUpdate];
        
        
    }
    return self;
}
-(void)update:(ccTime)delta
{
    KKInput *input = [KKInput sharedInput];
    for(int i=0;i<(int)[zipperButtons count];i++)
    {
        Zipper *temp = [zipperButtons objectAtIndex:i];
        
        if(temp.unzipped == true)
        {
            if(CGPointEqualToPoint(temp.end, resumeButton.end))
            {
                [self gameResumeChange];
            }
            if(CGPointEqualToPoint(temp.end, restartButton.end))
            {
                [self gameLayerChange];
            }
            if(CGPointEqualToPoint(temp.end, menuButton.end))
            {
                [self gameMenuChange];
            }
            [self removeChild:temp];
            [zipperButtons removeObject:temp];
            
            
            //[unzipped addObject:temp];
            
            
        }
        
    }
    if([input anyTouchBeganThisFrame])
    {
        touch = input.anyTouchLocation;
        for(int i=0;i<(int)[zipperButtons count];i++)
        {
            Zipper *temp = [zipperButtons objectAtIndex:i];
            if([self checkTouch:temp precision:50 precision:50])
            {
                zip = temp;
                //[self addChild:zip];
            }
        }
    }
    else if([input touchesAvailable])
    {
        touch = input.anyTouchLocation;
        if([self checkTouch:zip precision:150 precision:150])
        {
            [zip unzip:touch];
        }
    }
    
    
}

-(void)gameLayerChange
{
    [self pauseSchedulerAndActions];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFadeDown transitionWithDuration:.6f scene:[GameLayer scene]]];
}
-(void)gameResumeChange
{
    [self pauseSchedulerAndActions];
    [[CCDirector sharedDirector]popSceneWithTransition:[CCTransitionFade class] duration:0.6f];
}
-(void)gameMenuChange
{
    [self pauseSchedulerAndActions];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFadeDown transitionWithDuration:.6f scene:[StartLayer scene]]];
}

-(void)draw
{
    
    for(int i=0;i<(int)[zipperButtons count];i++)
    {
        Zipper *temp = [zipperButtons objectAtIndex:i];
        if(temp.unzipped == false)
        {
            [temp drawLine:self];
        }
        
        
    }
    
    
    
    
    //
    //    if(!CGPointEqualToPoint(touch, CGPointZero))
    //        acorn.position = touch;
}


-(bool)checkTouch:(Zipper*)zipper precision:(int)width precision:(int)length
{
    CGPoint cur = zipper.position;
    if(touch.x<=cur.x+width&&touch.x>=cur.x-width)
    {
        if(touch.y<=cur.y+length&&touch.y>=cur.y-length)
        {
            return true;
        }
    }
    
    return false;
}

+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	PauseLayer *layer = [PauseLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}



@end
