//
//  StartLayer.m
//  ZippersZippers
//
//  Created by Zaizen Kaegyoshi on 6/21/13.
//
//

#import "StartLayer.h"
#import "Zipper.h"
#import "GameLayer.h"
@implementation StartLayer

-(id)init
{
    if((self = [super init]))
    {
        glClearColor(.5f, 0, 0, .7f);
        zipperButtons = [NSMutableArray array];
        zipperButtonImgs = [NSMutableArray array];
        
        CCLabelTTF *welcomeSign = [CCLabelTTF labelWithString:@"Zippers" fontName:@"Georgia" fontSize:40];
        [self addChild:welcomeSign];
        welcomeSign.position = ccp(160,520);
        CCLabelTTF *welcomeSign2 = [CCLabelTTF labelWithString:@"Zippers!!!!" fontName:@"Georgia" fontSize:45];
        welcomeSign2.position = ccp(160,480);
        [self addChild:welcomeSign2];
        
       
        
        //Zipper buttons
        playButton = [Zipper zipper:ccp(106,300) end:ccp(222,301)precision:20];
        [zipperButtons addObject:playButton];
        CCSprite *playButtonLine = [playButton drawZipperLine];
        playButtonLine.zOrder = 6;
        [zipperButtonImgs addObject:playButtonLine];
        [self addChild:playButton z:7 tag:10];
        [self addChild:playButtonLine];
        
        CCLabelTTF *playText = [CCLabelTTF labelWithString:@"Play" fontName:@"Georgia" fontSize:30];
        playText.position = ccp(abs((playButton.end.x+playButton.start.x)/2),301);
        [self addChild:playText z:20];
        
        [self scheduleUpdate];
        
        
    }
    return self;
}

-(void)update:(ccTime)delta
{
    KKInput *input = [KKInput sharedInput];
    for(int i=0;i<(int)[zipperButtons count];i++)
    {
        Zipper *temp = [zipperButtons objectAtIndex:i];
        
        if(temp.unzipped == true)
        {
            if(CGPointEqualToPoint(temp.end, playButton.end))
            {
                [self gameLayerChange];
            }
            [self removeChild:temp];
            [zipperButtons removeObject:temp];
            
            
            //[unzipped addObject:temp];
            
                        
        }
        
    }
    if([input anyTouchBeganThisFrame])
    {
        touch = input.anyTouchLocation;
        for(int i=0;i<(int)[zipperButtons count];i++)
        {
            Zipper *temp = [zipperButtons objectAtIndex:i];
            if([self checkTouch:temp precision:50 precision:50])
            {
                zip = temp;
                //[self addChild:zip];
            }
        }
    }
    else if([input touchesAvailable])
    {
        touch = input.anyTouchLocation;
        if([self checkTouch:zip precision:150 precision:150])
        {
            [zip unzip:touch];
        }
    }


}
-(void)gameLayerChange
{
    [self pauseSchedulerAndActions];
    [[CCDirector sharedDirector]replaceScene:[CCTransitionFade transitionWithDuration:.6f scene:[GameLayer scene]]];
}

-(void)draw
{
    
    for(int i=0;i<(int)[zipperButtons count];i++)
    {
        Zipper *temp = [zipperButtons objectAtIndex:i];
        if(temp.unzipped == false)
        {
            [temp drawLine:self];
        }
        
        
    }
    
    
    
    
    //
    //    if(!CGPointEqualToPoint(touch, CGPointZero))
    //        acorn.position = touch;
}


-(bool)checkTouch:(Zipper*)zipper precision:(int)width precision:(int)length
{
    CGPoint cur = zipper.position;
    if(touch.x<=cur.x+width&&touch.x>=cur.x-width)
    {
        if(touch.y<=cur.y+length&&touch.y>=cur.y-length)
        {
            return true;
        }
    }
    
    return false;
}

+(id) scene
{
    CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	StartLayer *layer = [StartLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
@end
