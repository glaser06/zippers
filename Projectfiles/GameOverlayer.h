//
//  GameOverlayer.h
//  ZippersZippers
//
//  Created by Zaizen Kaegyoshi on 6/21/13.
//
//

#import "CCLayer.h"
#import "Zipper.h"
@interface GameOverlayer : CCLayer
{
    CGPoint touch;
    NSMutableArray *zipperButtons;
    Zipper *zip;
    Zipper *playButton;
    Zipper *menuButton;
}
+(id) scene;
@end
